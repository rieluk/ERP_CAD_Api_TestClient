﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ERP_CAD_Api_TestClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri("https://pcloud2.ebos.org/Documentation/ERP_CAD_Api/");
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            // Login
            await LogIn(httpClient, "demo", "demo");

            // create/update product with ID=10
            await SendProduct(httpClient, "10", "c:\\temp\\myTestFile.xml");

            // send a status update for element with ID=10
            await SendFeedback(httpClient, "10", "StatusOK");

            // attach a pdf to the element 10
            await SendDocument(httpClient, "10", "c:\\temp\\myTestFile.pdf", "PlanPDF");
        }

        private static async Task SendDocument(HttpClient httpClient, string id, string file, string documentType)
        {
            var bytes = File.ReadAllBytes(file);
            var payload = new
            {
                Document = bytes,
                DocumentType = "PXML"
            };

            var response = await httpClient.PostAsync($"ERP/Products/{id}/Documents", new StringContent(JsonConvert.SerializeObject(payload), Encoding.UTF8, "application/json"));

            Console.WriteLine($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
        }

        private static async Task SendProduct(HttpClient httpClient, string id, string file)
        {
            var bytes = File.ReadAllBytes(file);
            var payload = new
            {
                Product = bytes,
                DocumentType = "PXML",
                // through the modes you can send additional meta data
                Modes = new[] { new { Id = "id", Val = "value" } }
            };

            var response = await httpClient.PutAsync($"ERP/Products/{id}", new StringContent(JsonConvert.SerializeObject(payload), Encoding.UTF8, "application/json"));

            Console.WriteLine($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
        }

        private static async Task SendFeedback(HttpClient httpClient, string id, string code)
        {
            var payload = new { Code = code, ProdDate = DateTime.Now,
                // Some feedbacks need additional information
                FbVal = new[] { new { T = "T", V = "V" } } };

            var response = await httpClient.PostAsync($"ERP/Products/{id}/Feedbacks", new StringContent(JsonConvert.SerializeObject(payload), Encoding.UTF8, "application/json"));

            Console.WriteLine($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
        }

        protected static AuthenticationHeaderValue GetBasicHeader(string user, string pw)
        {
            var str = $"{user}:{pw}";
            var bytes = Encoding.UTF8.GetBytes(str);
            var base64 = Convert.ToBase64String(bytes);
            return new AuthenticationHeaderValue("Basic", base64);
        }
        
        async static Task LogIn(HttpClient client, string user, string pw)
        {
            client.DefaultRequestHeaders.Authorization = GetBasicHeader(user, pw);

            var result = await client.PostAsync("Authentication/login", null);
            Console.WriteLine($"{result.StatusCode}: {await result.Content.ReadAsStringAsync()}");
            if (!result.IsSuccessStatusCode)
                throw new Exception("login failed");
            var content = await result.Content.ReadAsStringAsync();
            var tokenString = JsonConvert.DeserializeObject<string>(content);

            client.DefaultRequestHeaders.Authorization = null;
            client.DefaultRequestHeaders.Add("Token", tokenString);
        }
    }
}
